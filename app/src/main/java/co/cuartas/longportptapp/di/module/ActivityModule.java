package co.cuartas.longportptapp.di.module;

import co.cuartas.longportptapp.presentation.asteroid.MainActivity;
import co.cuartas.longportptapp.presentation.user.LoginActivity;
import co.cuartas.longportptapp.presentation.user.RegisterActivity;
import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class ActivityModule {

    //@ContributesAndroidInjector(modules = {MainFragmentModule.class})
    @ContributesAndroidInjector
    abstract MainActivity contributeMainActivity();

    @ContributesAndroidInjector
    abstract LoginActivity contributeLoginActivity();

    @ContributesAndroidInjector
    abstract RegisterActivity contributeRegisterActivity();
}
