package co.cuartas.longportptapp.di.component;


import android.app.Application;

import javax.inject.Singleton;

import co.cuartas.longportptapp.base.BaseApplication;
import co.cuartas.longportptapp.data.AsteroidsDataBase;
import co.cuartas.longportptapp.di.module.ActivityModule;
import co.cuartas.longportptapp.di.module.ApplicationModule;
import co.cuartas.longportptapp.di.module.DBModule;
import co.cuartas.longportptapp.di.module.ViewModelModule;
import dagger.BindsInstance;
import dagger.Component;
import dagger.android.AndroidInjector;
import dagger.android.support.AndroidSupportInjectionModule;
import dagger.android.support.DaggerApplication;

@Singleton
@Component(modules = {
        DBModule.class,
        AndroidSupportInjectionModule.class,
        ActivityModule.class,
        ViewModelModule.class,
        ApplicationModule.class
})


public interface ApplicationComponent extends AndroidInjector<DaggerApplication> {

    void inject(BaseApplication application);

    @Component.Builder
    interface Builder{
        @BindsInstance
        Builder application(Application application);

        ApplicationComponent build();
    }
}
