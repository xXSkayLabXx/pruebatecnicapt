package co.cuartas.longportptapp.di.module;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.sqlite.db.SupportSQLiteDatabase;

import javax.inject.Singleton;

import co.cuartas.longportptapp.BuildConfig;
import co.cuartas.longportptapp.data.AsteroidsDataBase;
import dagger.Module;
import dagger.Provides;

@Module(includes = ViewModelModule.class)
public class DBModule {


    @Provides
    @Singleton
    AsteroidsDataBase provideAsteroidDataBase(Application application){
        return Room.databaseBuilder(application,

                AsteroidsDataBase.class, BuildConfig.DATA_BASE_NAME)
                .allowMainThreadQueries().addCallback(new RoomDatabase.Callback() {
                    @Override
                    public void onCreate(@NonNull SupportSQLiteDatabase db) {
                        super.onCreate(db);
                    }
                }).build();

    }
}
