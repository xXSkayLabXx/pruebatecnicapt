package co.cuartas.longportptapp.di.module;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

import co.cuartas.longportptapp.BuildConfig;
import co.cuartas.longportptapp.data.dao.UserDao;
import co.cuartas.longportptapp.domain.ImplRepository;
import co.cuartas.longportptapp.data.AsteroidsDataBase;
import co.cuartas.longportptapp.data.dao.AsteroidDao;
import co.cuartas.longportptapp.data.remote.ApiService;
import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class ApplicationModule {

    @Provides
    @Singleton
    ImplRepository provideImplRepository(ApiService apiService, Executor executor, AsteroidDao asteroidDao, UserDao userDao) {
        return new ImplRepository(apiService, executor, asteroidDao, userDao);
    }

    @Provides
    @Singleton
    AsteroidDao provideAsteroidDao(AsteroidsDataBase dataBase){
        return dataBase.asteroidDao();
    }

    @Provides
    @Singleton
    UserDao provideUserDao(AsteroidsDataBase dataBase){return  dataBase.userDao();}

    @Provides
    @Singleton
    Gson provideGson() {
        GsonBuilder gsonBuilder = new GsonBuilder();
        return gsonBuilder.create();
    }

    @Provides
    @Singleton
    Executor providesExecutor() {
        return Executors.newSingleThreadExecutor();
    }


    @Provides
    @Singleton
    OkHttpClient.Builder provideOkHttpClientBuilder() {
        return new OkHttpClient.Builder().connectTimeout(8, TimeUnit.SECONDS);
    }


    @Provides
    @Singleton
    OkHttpClient providesOkHttpClient(OkHttpClient.Builder okHttpClientBuilder) {

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.level(HttpLoggingInterceptor.Level.HEADERS);
        interceptor.level(HttpLoggingInterceptor.Level.BODY);

        okHttpClientBuilder.addInterceptor(new HttpLoggingInterceptor());

        return okHttpClientBuilder.cache(null).build();

    }

    @Provides
    @Singleton
    static Retrofit providesRetrofit(Gson gson, OkHttpClient okHttpClient) {
        return new Retrofit.Builder()
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .baseUrl(BuildConfig.BASE_URL)
                .build();
    }

    @Provides
    @Singleton
    ApiService providesService(Retrofit retrofit) {
        return retrofit.create(ApiService.class);
    }
}
