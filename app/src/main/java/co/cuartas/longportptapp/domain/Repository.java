package co.cuartas.longportptapp.domain;

import androidx.lifecycle.MutableLiveData;

import java.util.List;

import co.cuartas.longportptapp.data.entity.UserEntity;
import co.cuartas.longportptapp.data.remote.dto.Root;
import co.cuartas.longportptapp.data.entity.AsteroidsEntity;

public interface Repository {

    MutableLiveData<Root> getAsteroidsFromInternet();

    void saveAsteroidsToDB(List<AsteroidsEntity> asteroidsList);
    void saveUserToDB(UserEntity user);

    MutableLiveData<List<AsteroidsEntity>> getAsteroidsFromDB(String idUser);

    MutableLiveData<UserEntity> getUserFromDB(String email, String password);
}
