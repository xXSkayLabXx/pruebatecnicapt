package co.cuartas.longportptapp.domain;

import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.List;
import java.util.concurrent.Executor;

import javax.inject.Inject;
import javax.inject.Singleton;

import co.cuartas.longportptapp.BuildConfig;
import co.cuartas.longportptapp.data.dao.AsteroidDao;
import co.cuartas.longportptapp.data.dao.UserDao;
import co.cuartas.longportptapp.data.entity.UserEntity;
import co.cuartas.longportptapp.data.remote.ApiService;
import co.cuartas.longportptapp.data.remote.dto.Root;
import co.cuartas.longportptapp.data.entity.AsteroidsEntity;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

@Singleton
public class ImplRepository implements Repository {

    private final ApiService apiService;
    private final CompositeDisposable compositeDisposable = new CompositeDisposable();
    private final Executor executor;
    private final AsteroidDao asteroidDao;
    private final UserDao userDao;
    @Inject
    public ImplRepository(ApiService apiService, Executor executor, AsteroidDao asteroidDao, UserDao userDao) {
        this.apiService = apiService;
        this.executor = executor;
        this.asteroidDao = asteroidDao;
        this.userDao = userDao;
    }

    @Override
    public MutableLiveData<Root> getAsteroidsFromInternet() {
        MutableLiveData<Root> response = new MutableLiveData<>();
        executor.execute(() -> compositeDisposable.add(
                apiService.getAsteroidsFromInternet("2023-05-04", "2023-05-04", BuildConfig.API_KEY)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(response::setValue,
                                throwable -> {response.setValue(null);
                                    Log.e(throwable.toString(),throwable.getMessage());
                        })

        ));
        return response;
    }

    @Override
    public void saveAsteroidsToDB(List<AsteroidsEntity> asteroidsList) {
        asteroidDao.delete();
       asteroidDao.insert(asteroidsList);
    }

    @Override
    public void saveUserToDB(UserEntity user) {
        if(user !=null){
            executor.execute(() -> {
                userDao.insert(user);
            });
        }
    }

    @Override
    public MutableLiveData<List<AsteroidsEntity>> getAsteroidsFromDB(String idUser) {
        MutableLiveData<List<AsteroidsEntity>> listBD = new MutableLiveData<>();
        listBD.setValue(asteroidDao.getAsteroidsFromUser(idUser));
        return listBD;

    }

    @Override
    public MutableLiveData<UserEntity> getUserFromDB(String email, String password) {
        MutableLiveData<UserEntity> userBD = new MutableLiveData<>();
        userBD.setValue(userDao.getUser(email,password));
        return  userBD;
    }
}
