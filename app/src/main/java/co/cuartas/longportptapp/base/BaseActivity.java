package co.cuartas.longportptapp.base;


import android.os.Bundle;

import androidx.annotation.LayoutRes;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;

import java.util.Objects;

import dagger.android.support.DaggerAppCompatActivity;

public abstract class BaseActivity extends DaggerAppCompatActivity {

    private Toolbar toolbar;


    @LayoutRes
    protected abstract int layoutRes();

    protected abstract int toolbarId();


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(layoutRes());
        setToolbar(null);

    }

    public void setToolbar(@Nullable Toolbar toolbar) {
        if (toolbar == null) {
            this.toolbar = findViewById(toolbarId());
        }
        if (this.toolbar != null) {
            setSupportActionBar(this.toolbar);
        }
    }

    public Toolbar getToolbar() {
        return toolbar;
    }

    public void setSupportActionBar(boolean displayHome, boolean showIconHome) {
        if (toolbar != null) {
            Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(displayHome);
            getSupportActionBar().setDisplayShowHomeEnabled(showIconHome);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
