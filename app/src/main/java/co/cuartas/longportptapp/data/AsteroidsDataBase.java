package co.cuartas.longportptapp.data;


import androidx.room.Database;
import androidx.room.RoomDatabase;

import co.cuartas.longportptapp.data.dao.AsteroidDao;
import co.cuartas.longportptapp.data.dao.UserDao;
import co.cuartas.longportptapp.data.entity.AsteroidsEntity;
import co.cuartas.longportptapp.data.entity.UserEntity;

@Database(entities = {AsteroidsEntity.class, UserEntity.class},
        version = 1,
        exportSchema = false)
public abstract class AsteroidsDataBase extends RoomDatabase {

    public abstract AsteroidDao asteroidDao();

    public abstract UserDao userDao();
}
