package co.cuartas.longportptapp.data.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import java.util.List;

import co.cuartas.longportptapp.data.entity.AsteroidsEntity;

@Dao
public interface AsteroidDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(List<AsteroidsEntity> asteroids);

    @Query("SELECT * from asteroids WHERE id_user =:idUser")
    List<AsteroidsEntity> getAsteroidsFromUser(String idUser);


    @Query("DELETE FROM asteroids")
    void delete();
}
