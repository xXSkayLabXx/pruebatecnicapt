package co.cuartas.longportptapp.data.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;


import co.cuartas.longportptapp.data.entity.UserEntity;

@Dao
public interface UserDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(UserEntity user);

    @Query("SELECT * from users WHERE email =:email AND password=:password")
    UserEntity getUser(String email, String password);


    @Query("DELETE FROM users")
    void delete();
}
