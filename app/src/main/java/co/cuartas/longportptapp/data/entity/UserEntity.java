package co.cuartas.longportptapp.data.entity;

import androidx.room.Entity;
import androidx.room.Index;
import androidx.room.PrimaryKey;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@RequiredArgsConstructor
@AllArgsConstructor
@Entity(tableName = "Users", indices = {@Index(value = "id", unique = true)})
public final class UserEntity {

    @PrimaryKey
    public Integer id;

    public String identification;
    public String first_name;
    public String last_name;
    public String email;
    public String password;
    public String created_at;
    public String update_at;


}
