package co.cuartas.longportptapp.data.remote.dto;


import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@RequiredArgsConstructor
@AllArgsConstructor
public class Kilometers implements Serializable {

    public double estimated_diameter_min;
    public double estimated_diameter_max;
}
