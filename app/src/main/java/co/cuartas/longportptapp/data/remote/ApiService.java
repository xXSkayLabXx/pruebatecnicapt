package co.cuartas.longportptapp.data.remote;

import co.cuartas.longportptapp.data.remote.dto.Root;
import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ApiService {

    @GET("/neo/rest/v1/feed?")
    Single<Root> getAsteroidsFromInternet(@Query("start_date") String start_date,
                                          @Query("end_date") String end_date,
                                          @Query("api_key") String api_key);
}
