package co.cuartas.longportptapp.data.remote.dto;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@RequiredArgsConstructor
@AllArgsConstructor
public class CloseApproachDatum implements Serializable {

    public String close_approach_date_full;
    public RelativeVelocity relative_velocity;
}
