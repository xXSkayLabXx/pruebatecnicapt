package co.cuartas.longportptapp.data.remote.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@RequiredArgsConstructor
@AllArgsConstructor
public class _20230504  implements Serializable {

    public String id;
    public String neo_reference_id;
    public String name;
    public boolean is_potentially_hazardous_asteroid;
    public EstimatedDiameter estimated_diameter;
    public List<CloseApproachDatum> close_approach_data;
}
