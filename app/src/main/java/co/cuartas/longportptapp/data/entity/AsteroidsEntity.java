package co.cuartas.longportptapp.data.entity;


import androidx.room.Entity;
import androidx.room.Index;
import androidx.room.PrimaryKey;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@RequiredArgsConstructor
@AllArgsConstructor
@Entity(tableName = "Asteroids", indices = {@Index(value = "id", unique = true)})
public final class AsteroidsEntity {

    @PrimaryKey
    public Integer id;

    public String id_asteroid;
    public String id_user;
    public String neo_reference_id;
    public String name;
    public boolean is_potentially_hazardous_asteroid;

    public double estimated_diameter_min;
    public double estimated_diameter_max;

    public String close_approach_date_full;

    public String kilometers_per_hour;
}
