package co.cuartas.longportptapp.presentation.user;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.lifecycle.ViewModelProvider;

import com.google.android.material.progressindicator.CircularProgressIndicator;

import javax.inject.Inject;

import co.cuartas.longportptapp.R;
import co.cuartas.longportptapp.base.BaseActivity;
import co.cuartas.longportptapp.presentation.MainViewModel;
import co.cuartas.longportptapp.presentation.asteroid.MainActivity;


public class LoginActivity extends BaseActivity {

    @Inject
    ViewModelProvider.Factory factory;
    MainViewModel viewModel;

    private EditText etUserEmailLogin, etPasswordLogin;
    private TextView etRegistrar;
    private Button btn_login;

    private CircularProgressIndicator indicator;


    @Override
    protected int layoutRes() {
        return R.layout.activity_login;
    }

    @Override
    protected int toolbarId() {
        return 0;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        viewModel = new ViewModelProvider(getViewModelStore(),factory).get(MainViewModel.class);

        etUserEmailLogin = findViewById(R.id.userEmail);
        etPasswordLogin = findViewById(R.id.password);
        etRegistrar = findViewById(R.id.etRegistrar);
        btn_login = findViewById(R.id.btn_login);
        indicator = findViewById(R.id.circularProgress);

        btn_login.setOnClickListener(view -> loginSession(etUserEmailLogin.getText().toString(), etPasswordLogin.getText().toString(), indicator));
        etRegistrar.setOnClickListener(view -> registerUser());

    }

    private void registerUser(){
        Intent intent = new Intent(LoginActivity.this, RegisterActivity.class);
        startActivity(intent);
        finish();
    }

    private void loginSession(String email, String password,CircularProgressIndicator indicator){
        if( email.isEmpty()){
            Toast.makeText(this, "El campo email esta vacio", Toast.LENGTH_SHORT).show();
        } else if (password.isEmpty()) {
            Toast.makeText(this, "El campo password esta vacio", Toast.LENGTH_SHORT).show();
        }else{
            indicator.setVisibility(View.VISIBLE);
            viewModel.getUserFromDB(email,password).observe(this, user ->{
                if(user == null){
                    Toast.makeText(this, "Datos erroneos", Toast.LENGTH_LONG).show();
                    etUserEmailLogin.setText("");
                    etPasswordLogin.setText("");
                    indicator.setVisibility(View.GONE);
                }else {
                    indicator.setVisibility(View.GONE);
                    new Handler().postDelayed(() -> {
                    Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                    intent.putExtra("user",user.id.toString());
                    startActivity(intent);
                    finish();
                    }, 2000);
                }
            });
        }

    }

}