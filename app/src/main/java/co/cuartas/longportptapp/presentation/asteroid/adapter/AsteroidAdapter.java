package co.cuartas.longportptapp.presentation.asteroid.adapter;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;

import java.util.List;

import co.cuartas.longportptapp.R;
import co.cuartas.longportptapp.data.entity.AsteroidsEntity;
import co.cuartas.longportptapp.presentation.asteroid.MainActivity;
import co.cuartas.longportptapp.presentation.detail.DetailActivity;
import co.cuartas.longportptapp.presentation.user.LoginActivity;

public class AsteroidAdapter extends RecyclerView.Adapter<AsteroidAdapter.ViewHolder>{

    private final AppCompatActivity activity;
    private final List<? extends AsteroidsEntity> list;

    public AsteroidAdapter(AppCompatActivity activity, List<? extends AsteroidsEntity> list) {
        this.activity = activity;
        this.list = list;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_asteroid, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.param1.setText("Nombre: "+list.get(position).name);
        holder.param2.setText("id: "+list.get(position).id_asteroid);
    }

    @Override
    public int getItemCount() {
        return list !=null ? list.size() : 0;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        private final TextView param1, param2;
        private final CardView backgroundCard;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            param1 = itemView.findViewById(R.id.param1);
            param2 = itemView.findViewById(R.id.param2);
            backgroundCard = itemView.findViewById(R.id.cvAsteroid);

        }
    }
}
