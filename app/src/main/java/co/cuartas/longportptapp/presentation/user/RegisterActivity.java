package co.cuartas.longportptapp.presentation.user;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.progressindicator.CircularProgressIndicator;

import java.util.Calendar;

import javax.inject.Inject;

import co.cuartas.longportptapp.R;
import co.cuartas.longportptapp.base.BaseActivity;
import co.cuartas.longportptapp.data.entity.UserEntity;
import co.cuartas.longportptapp.presentation.MainViewModel;
import co.cuartas.longportptapp.presentation.asteroid.MainActivity;

public class RegisterActivity extends BaseActivity {

    @Inject
    ViewModelProvider.Factory factory;
    MainViewModel viewModel;

    private EditText etFirstName, etLastName, etEmail, etPassword, etIdentification;


    @Override
    protected int layoutRes() {
        return R.layout.activity_register;
    }

    @Override
    protected int toolbarId() {
        return 0;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        viewModel = new ViewModelProvider(getViewModelStore(),factory).get(MainViewModel.class);

        etFirstName = findViewById(R.id.etFirstName);
        etLastName = findViewById(R.id.etLastName);
        etEmail = findViewById(R.id.etEmail);
        etPassword = findViewById(R.id.etPassword);
        etIdentification = findViewById(R.id.etIdentification);

        Button btn_register = findViewById(R.id.btn_register);

        TextView btn_back = findViewById(R.id.btn_back);

        CircularProgressIndicator indicator = findViewById(R.id.circularProgress);

        btn_register.setOnClickListener(view -> registrarDataUser(indicator) );

        btn_back.setOnClickListener(view -> loginSession());

    }


    private void loginSession(){
        Intent intent = new Intent(RegisterActivity.this, LoginActivity.class);
        startActivity(intent);
        finish();
    }

    private void registrarDataUser(CircularProgressIndicator indicator){
        indicator.setVisibility(View.VISIBLE);

        //creamos las variables las inicializamos y las convertimos
        String first_name = etFirstName.getText().toString();
        String last_name = etLastName.getText().toString();
        String email = etEmail.getText().toString();
        String password = etPassword.getText().toString();
        String identification = etIdentification.getText().toString();


        if(first_name.isEmpty()){
            Toast.makeText(this, "Nombre no puede estar vacio", Toast.LENGTH_SHORT).show();
        } else if (last_name.isEmpty()) {
            Toast.makeText(this, "Apellido no puede estar vacio", Toast.LENGTH_SHORT).show();
        } else if (email.isEmpty()) {
            Toast.makeText(this, "Email no puede estar vacio", Toast.LENGTH_SHORT).show();
        } else if (password.isEmpty()) {
            Toast.makeText(this, "Password no puede estar vacio", Toast.LENGTH_SHORT).show();
        } else if (identification.isEmpty()) {
            Toast.makeText(this, "Identificacion no puede estar vacio", Toast.LENGTH_SHORT).show();
        }else {

            UserEntity user = new UserEntity();
            user.first_name = first_name;
            user.last_name = last_name;
            user.identification = identification;
            user.password = password;
            user.email = email;
            user.created_at = Calendar.getInstance().getTime().toString();
            user.update_at = Calendar.getInstance().getTime().toString();

            viewModel.saveUserBD(user);
            Toast.makeText(this, "Usuario registrado", Toast.LENGTH_SHORT).show();

            indicator.setVisibility(View.GONE);
            new Handler().postDelayed(() -> {
                Intent intent = new Intent(RegisterActivity.this, LoginActivity.class);
                startActivity(intent);
                finish();
            }, 2000);

        }

    }
}