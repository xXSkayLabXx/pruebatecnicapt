package co.cuartas.longportptapp.presentation.asteroid;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.os.Handler;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.google.android.material.progressindicator.CircularProgressIndicator;

import java.util.List;

import javax.inject.Inject;

import co.cuartas.longportptapp.R;
import co.cuartas.longportptapp.base.BaseActivity;
import co.cuartas.longportptapp.data.entity.AsteroidsEntity;
import co.cuartas.longportptapp.presentation.MainViewModel;
import co.cuartas.longportptapp.presentation.asteroid.adapter.AsteroidAdapter;

public class MainActivity extends BaseActivity {

    private boolean pressMenuItem = false;

    @Inject
    ViewModelProvider.Factory factory;
    MainViewModel viewModel;
    private String id;

    @Override
    protected int layoutRes() {
        return R.layout.activity_main;
    }

    @Override
    protected int toolbarId() {
        return R.id.toolbar_material;
    }



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewModel = new ViewModelProvider(getViewModelStore(),factory).get(MainViewModel.class);

        setSupportActionBar(false, true);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
             id = extras.getString("user");

            //The key argument here must match that used in the other activity
        }
        System.out.println(id);

        CircularProgressIndicator indicator = findViewById(R.id.circularProgress);

        indicator.setVisibility(View.VISIBLE);

        viewModel.getAsteroidsFromInternet().observe(this, root -> {

            if(root.near_earth_objects != null){
                getToolbar().setTitle("Asteroids");
                viewModel.saveAsteroidsToDB(root.near_earth_objects,id);
                viewModel.getAsteroidsFromDB(id).observe(this, asteroidsEntities -> loadRecyclerView(asteroidsEntities,indicator));
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (!pressMenuItem) {
            if (item.getItemId() == R.id.exit) {
                pressMenuItem = true;
                finish();
                new Handler().postDelayed(() -> pressMenuItem = false, 400);
            }
        }
        return super.onOptionsItemSelected(item);
    }

    private void loadRecyclerView(List<AsteroidsEntity> list, CircularProgressIndicator indicator) {
        RecyclerView recyclerView = findViewById(R.id.rvAsteroid);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        AsteroidAdapter adapter = new AsteroidAdapter(MainActivity.this,list);
        indicator.setVisibility(View.GONE);
        recyclerView.setAdapter(adapter);
        adapter.notifyDataSetChanged();

    }
}