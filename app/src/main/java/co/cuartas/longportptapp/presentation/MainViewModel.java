package co.cuartas.longportptapp.presentation;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import co.cuartas.longportptapp.data.entity.AsteroidsEntity;
import co.cuartas.longportptapp.data.entity.UserEntity;
import co.cuartas.longportptapp.data.remote.dto.NearEarthObjects;
import co.cuartas.longportptapp.data.remote.dto.Root;
import co.cuartas.longportptapp.data.remote.dto._20230504;
import co.cuartas.longportptapp.domain.ImplRepository;

public class MainViewModel extends ViewModel {

    private final ImplRepository repository;
    private MutableLiveData<Root> asteroids;


    private MutableLiveData<UserEntity> user;
    private MutableLiveData<List<AsteroidsEntity>> asteroidsEntity;


    @Inject
    public MainViewModel(ImplRepository repository) {
        this.repository = repository;
    }


    public LiveData<Root> getAsteroidsFromInternet(){
        asteroids = new MutableLiveData<>();
        asteroids = repository.getAsteroidsFromInternet();
        return asteroids;
    }


    public LiveData<List<AsteroidsEntity>> getAsteroidsFromDB(String idUser){
        asteroidsEntity = new MutableLiveData<>();
        asteroidsEntity = repository.getAsteroidsFromDB(idUser);
        return asteroidsEntity;
    }

    public LiveData<UserEntity> getUserFromDB(String email, String password){
        user= new MutableLiveData<>();
        user = repository.getUserFromDB(email,password);
        return user;
    }

    public void saveUserBD(UserEntity user){
        repository.saveUserToDB(user);
    }

    public void saveAsteroidsToDB(NearEarthObjects objects, String idUser) {

        List<AsteroidsEntity> entityList = new ArrayList<>();

            for (_20230504 earthObjects : objects._2023_05_04) {
                AsteroidsEntity entity = new AsteroidsEntity();
                entity.id_asteroid = earthObjects.id;
                entity.id_user = idUser;
                entity.neo_reference_id = earthObjects.neo_reference_id;
                entity.name = earthObjects.name;
                entity.is_potentially_hazardous_asteroid = earthObjects.is_potentially_hazardous_asteroid;
                entity.estimated_diameter_min = earthObjects.estimated_diameter.kilometers.estimated_diameter_min;
                entity.estimated_diameter_max = earthObjects.estimated_diameter.kilometers.estimated_diameter_max;
                entity.close_approach_date_full = earthObjects.close_approach_data.get(0).close_approach_date_full;
                entity.kilometers_per_hour = earthObjects.close_approach_data.get(0).relative_velocity.kilometers_per_hour;
                entityList.add(entity);
            }
        repository.saveAsteroidsToDB(entityList);
    }

}
