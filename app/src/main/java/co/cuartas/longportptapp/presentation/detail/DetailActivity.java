package co.cuartas.longportptapp.presentation.detail;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import co.cuartas.longportptapp.R;
import co.cuartas.longportptapp.base.BaseActivity;

public class DetailActivity extends BaseActivity {

    @Override
    protected int layoutRes() {
        return R.layout.activity_detail;
    }

    @Override
    protected int toolbarId() {
        return 0;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
}