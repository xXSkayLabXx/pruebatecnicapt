
# NASA ASTEROIDS - LONGPORTAVIATION

- Consumo de [ Asteroids - NeoWs ](https://api.nasa.gov/).
- Creacion de usuarios.
- Validación de campos vacios.
- Login de usuarios.
- Lista de asteroides de acuerdo al id del usuario.
- Lista de asteroides con nombre y id.
- Visualizar detalle de los asteroides.
- Cerrar sesión.
- Guardar el registro de usuarios y lista de asteroides en `Room`.

La app esta implementada sobre[`JitPack Android`](https://jitpack.io/docs/ANDROID/).


## Diseño UX/UI

|                                                                                                           |                                                                                                              |                                                                                                               |
|:---------------------------------------------------------------------------------------------------------:|:------------------------------------------------------------------------------------------------------------:|:-------------------------------------------------------------------------------------------------------------:|
| <img alt="Screen 1" src="https://gitlab.com/xXSkayLabXx/pruebatecnicapt/blob/main/screenshots/Login.png"> | <img alt="Screen 2" src="https://gitlab.com/xXSkayLabXx/pruebatecnicapt/blob/main/screenshots/Register.png"> | <img alt="Screen 3" src="https://gitlab.com/xXSkayLabXx/pruebatecnicapt/blob/main/screenshots/Asteroids.png"> |
| <img alt="Screen 4" src="https://gitlab.com/xXSkayLabXx/pruebatecnicapt/blob/main/screenshots/Exit.png">  |



## Acciones consumidas

- Ateroids:  [GET https://api.nasa.gov/neo/rest/v1/feed?start_date=START_DATE&end_date=END_DATE&api_key=API_KEY](https://api.nasa.gov/neo/rest/v1/feed?start_date=2023-05-04&end_date=2023-05-04&api_key=DMaihrIZkAHqoU8jQnqgg48ebi3m9gfepJ7Xf4Sa)

## DATA SOURCE


**DATA BASE**

```java
@Database(entities = {AsteroidsEntity.class, UserEntity.class},
        version = 1,
        exportSchema = false)
public abstract class AsteroidsDataBase extends RoomDatabase {

    public abstract AsteroidDao asteroidDao();

    public abstract UserDao userDao();
}
```

**REMOTE**

```java
public interface ApiService {

    @GET("/neo/rest/v1/feed?")
    Single<Root> getAsteroidsFromInternet(@Query("start_date") String start_date,
                                          @Query("end_date") String end_date,
                                          @Query("api_key") String api_key);
}
```

## GRADLE

**CONFIG GRADLE**

```groovy
defaultConfig {
        applicationId "co.cuartas.longportptapp"
        minSdk 23
        targetSdk 33
        versionCode 1
        versionName "1.0"

        testInstrumentationRunner "androidx.test.runner.AndroidJUnitRunner"

        buildConfigField "String", "BASE_URL", "\"https://api.nasa.gov\""
        buildConfigField "String", "DATA_BASE_NAME", "\"asteroids_db\""
        buildConfigField "String", "API_KEY", "\"DMaihrIZkAHqoU8jQnqgg48ebi3m9gfepJ7Xf4Sa\""

        vectorDrawables.useSupportLibrary = true
        multiDexEnabled true
    }
```

**DEPENDENCIES GRADLE**

```groovy
dependencies {

    implementation fileTree(dir: 'libs', include: ['*.jar'])

    //Design
    implementation 'androidx.appcompat:appcompat:1.3.1'
    implementation 'androidx.constraintlayout:constraintlayout:2.1.0'
    implementation 'com.google.android.material:material:1.6.0'
    implementation 'androidx.legacy:legacy-support-v4:1.0.0'
    implementation 'androidx.recyclerview:recyclerview:1.2.1'
    implementation 'androidx.cardview:cardview:1.0.0'

    testImplementation 'junit:junit:4.13.2'
    androidTestImplementation 'androidx.test.ext:junit:1.1.5'
    androidTestImplementation 'androidx.test.espresso:espresso-core:3.5.1'

    // ViewModel and LiveData
    implementation "androidx.lifecycle:lifecycle-viewmodel:$lifecycle_version"
    implementation "androidx.lifecycle:lifecycle-livedata:$lifecycle_version"

    implementation "androidx.lifecycle:lifecycle-viewmodel-savedstate:$lifecycle_version"
    // alternately - if using Java8, use the following instead of lifecycle-compiler
    implementation "androidx.lifecycle:lifecycle-common-java8:$lifecycle_version"

    implementation 'androidx.fragment:fragment:1.3.6'

    //Room
    implementation 'androidx.room:room-runtime:2.4.0-alpha04'
    annotationProcessor 'androidx.room:room-compiler:2.4.0-alpha04'

    // Gson
    implementation "com.google.code.gson:gson:2.8.6"

    // Okhttp3
    implementation "com.squareup.okhttp3:okhttp:4.9.0"
    implementation 'com.squareup.okhttp3:okhttp-urlconnection:3.2.0'
    implementation 'com.squareup.okhttp3:logging-interceptor:4.7.2'
    implementation 'com.squareup.okhttp3:okhttp-tls:4.8.1'

    // Retrofit 2
    implementation 'com.squareup.retrofit2:adapter-rxjava2:2.7.0'
    implementation 'com.squareup.retrofit2:retrofit:2.9.0'
    implementation 'com.squareup.retrofit2:converter-gson:2.9.0'
    implementation 'com.squareup.retrofit2:converter-jackson:2.7.0'

    //RX2
    implementation 'io.reactivex.rxjava2:rxandroid:2.1.1'
    implementation 'io.reactivex.rxjava2:rxjava:2.2.20'

    //dagger 2
    implementation 'com.google.dagger:dagger:2.35.1'
    implementation 'com.google.dagger:dagger-android:2.35.1'
    implementation 'com.google.dagger:dagger-android-support:2.30.1'
    annotationProcessor 'com.google.dagger:dagger-compiler:2.31.2'
    annotationProcessor 'com.google.dagger:dagger-android-processor:2.30.1'

    implementation 'org.jetbrains:annotations:16.0.1'
    implementation 'androidx.multidex:multidex:2.0.1'

    implementation 'com.google.code.findbugs:jsr305:3.0.2'
    
    // Lombok
    compileOnly 'org.projectlombok:lombok:1.18.16'
    annotationProcessor 'org.projectlombok:lombok:1.18.16'
}
```    
